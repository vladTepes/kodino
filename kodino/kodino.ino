/* aqui
 * Kodino
 * arduino + Live Coding += Kodino
 * v1.0
 * 
 */

int PIN_CHANNEL = 2;
const int BUF_LENGTH = 80;
char character[BUF_LENGTH];
int t;
struct duty {
  float h;
  float l;
};

duty duty_hl;

void setup() {
  Serial.begin(9600);
}

void loop() {
  
  if(readChannel(Serial.read(), character, BUF_LENGTH) > 0){
    Serial.println(character);
  }
  
  int hz = atoi(character);
  float t = 1000000/(hz );

  duty_hl.h = 0.25;
  duty_hl.l = (1 - duty_hl.h);
  
  digitalWrite(PIN_CHANNEL, HIGH);
  delayMicroseconds(t*duty_hl.h);
  digitalWrite(PIN_CHANNEL, LOW);
  delayMicroseconds(t*duty_hl.l);
    
  //delay(500);
}

int readChannel(int readch, char *buf, int lenbuf) {
    static int pos = 0;
    int rpos;

    if (readch > 0) {
        switch (readch) {
            case '\r': // Ignore CR
                break;
            case '\n': // Return on new-line
                rpos = pos;
                pos = 0;  // Reset position index ready for next time
                return rpos;
            default:
                if (pos < lenbuf-1) {
                    buf[pos++] = readch;
                    buf[pos] = 0;
                }
        }
    }
    return 0;
}
